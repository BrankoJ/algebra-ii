\section{Preliminaries}
Recommended books for the lecture are: 
\begin{itemize}
	\item Hungerford -- Algebra
	\item Knapp -- Basic Algebra
	\item Knapp -- Advanced Algebra
	\item Procesi -- Lie Gruoups: An approach through invariant theory
	\item Borel -- Linear Algebraic Groups
	\item Humphreys -- Linear Algebraic Groups
	\item Springer -- Linear Algebraic Groups
\end{itemize}
\section{Group actions}
If $G$ is a group, denote by $e \in G$ the neutral element, by $g^{-1}$ the inverse of $g \in G$ and by $gh$ the composition $g \circ h$.
\begin{definition}
	Given a group $G$ and a set $X$, an action of $G$ on $X$ is a map 
	\begin{align*}
		\pi \colon G \times X & \longrightarrow X \\
		(g,x) & \longrightarrow g.x
	\end{align*}
	such that
	\begin{itemize}
		\item[(A1)] $e.x=x$
		\item[(A2)] $(gh).x=g.(h.x)$
	\end{itemize}
	for all $x \in X$ and $g,h \in G$.
\end{definition}
\begin{definition}
	Given a set $X$, define $$S(X)=\{f: X \to X | f \text{ bijective}\},$$ the symmetric group of $X$ (with composition as group multiplication).
	
	Given a $G$-set $X$ and $g \in G$. Let $\pi_G \in S(X)$ be defined as $\pi_g(x)=g.x$.
	
	We have $\pi_g \in S(X)$ since $\pi_{g^{-1}}$ is the inverse map because $$\pi_{g^{-1}} \circ \pi_g (x) = (g^{-1} g).x = x = \pi_g\pi_{g^{-1}}$$ for all $x$.
\end{definition}
\begin{lemma} \label{lemI.1}
	For any group $G$ and a set $X$, we have a $1:1$-correspondence 
	\begin{align*}
		\{G-\text{action on } X \} & \longleftrightarrow  \{\text{group homos } G \to S(X)\} \\
		(\pi: G \times X \to X) & \mapsto \hat \pi \\
		\mathring{\varphi} & \mapsfrom \varphi
	\end{align*}
	where $\hat \pi (g)(x) = \pi((g.x))=g.x$ and $\mathring{\varphi}((g.x)):=\varphi(g)(x)$ for all $g \in  G, x \in X$. 
\begin{proof}
	Do it yourself!
\end{proof}
\end{lemma}
\begin{example}~ \vspace{-\topsep}
	\begin{enumerate}[label=(\Roman*)]
		\item $G$ acts on itseld by
		\begin{itemize}
			\item left multiplication: $g.x=gx$
			\item right multiplication: $g.x=xg^{-1}$
			\item conjugation $g.x=gxg^{-1}$
		\end{itemize}
		\item Any set is a $G$-set via the trivial action $g.x=x$ for all $g \in G, x \in X$
		\item $X,Y$ $G$-sets. Then $G$ acts on $$\text{Maps}(X,Y):=\{f: X \to Y \text{ a map}\}$$ via $(g.f)(x)=g.(f(g^{-1}.x))$ for $f \in \text{Maps}(X,Y), g \in G, x \in X$.
		
		Special case: The action on $Y$ is trivial, then $(g.f)(x)=f(g^{-1}.x)$.
	\end{enumerate}
\end{example}
\begin{definition}
	Let $X,Y$ be $G$-sets. A map $f: X \to Y$ is called $G$-equivariant if $f(g.x)=g.(f(x))$ for all $g \in G, x \in X$.
	
	We write $$\Hom_G(X,Y):= \{f: X \to Y | f \text{ is a } G-\text{equiv.}\} \; .$$
\end{definition}
\begin{lemma}\label{lemI.2}
	Let $G$ be a group. 
	\begin{enumerate}
		\item If $X$ is a $G$-set, then $\id_X \in Hom_G(X,X)$.
		\item If $X,Y,Z$ are $G$-sets, $f_1 \in \Hom_G(X,Y), f_2 \in \Hom_G(Y,Z)$, \newline then $f_2 \circ f_1 \in \Hom_G(X,Z)$.
	\end{enumerate}
\end{lemma}
\begin{example}
	Let $G$ be a group.
	\begin{enumerate}[label=(\roman*)]
		\item If $G$ acts on itself by left multiplication then
		\begin{align*}
			\Hom_G(G,G) & \cong G & \text{(as sets)} \\
			f & \mapsto f(e) \\
			m_a & \mapsfrom a 
		\end{align*}
		where $m_a:=\text{right multiplication}$.
		
		\textit{Proof.} well-definedness: $m_a(g.x)=m_a(gx) = (gx)(a)=g(xa)=g.m_a(x)$.
		Maps are inverse to each other:  Check on your own.
		\item If $X,Y$ are trivial $G$-sets then $\Hom_G(X,Y) = \Maps(X,Y)$.
	\end{enumerate}
\end{example}
\begin{definition}
	Let $X$ be a $G$-set. We write $$\orbit{G}{X}:=\{\text{orbits of } G-\text{action on }X\}$$
	where the orbits are given by $\mathcal{O}_x=\{g.x | g \in G\}$ for any $x \in X$.
\end{definition}
\begin{note}
	We have $\mathcal{O}_x=\mathcal{O}_y$ if $y \in \mathcal{O}_x$ since $g^{-1}.(g.x)=x$.
\end{note}
\begin{remark}
	We can view $\orbit{G}{X}$ as a $G$-set via the trivial action , Then 
	\begin{align*}
		\can: X & \mapsto \orbit{G}{X} \\ 
		g & \mapsto \mathcal{O}_g
	\end{align*}
	is a $G$-equiv. since $\can(g.y)=\mathcal{O}_{g.y}=\mathcal{O}_y=\can(y)=g.\can(y)$.
\end{remark}
\begin{definition}
	Let $X$ be a $G$-set. Then $X^G:= \{x \in X |  \forall g \in G: g.x=x\}$ is the set of $G$-fixed points or $G$-invariants in $X$. 
\end{definition}
\begin{lemma} \label{lemI.3}
	Let $X,Y$ be $G$-sets, $f \in \Hom_G(X,Y)$, then $f(X^G) \subset Y^G$.
	\begin{proof}
		$x \in X^G \Rightarrow g.(f(x))=f(g.x)=f(x) \Rightarrow f(x) \in Y^G$.
	\end{proof}
\end{lemma}
Thus, $f$ induces a map $f^G: X^G \to Y^G$ by restriction.
\begin{lemma} \label{lemI.4}
	Let $G$ be a group.
	\begin{enumerate}[label=(\roman*)]
		\item If $X$ is a $G$-set then $\id_X^G=\id_{X^G}$
		\item If $X,Y,Z$ are $G$-sets and $f_1 \in \Hom_G(X,Y), f_2 \in \Hom_G(Y,Z)$ then $$(f_2 \circ f_1)^G O f_2^G \circ f^1(G).$$
	\end{enumerate}
	\begin{proof}
		\begin{enumerate}[label=(\roman*)]
			\item obvious
			\item $f_2^G \circ f_1^G = f_2|_{Y^G} \circ f_1|_{X^G}=f_2 \circ f_1|_{X^G} = (f_2 \circ f_1)^G$
		\end{enumerate}
	\end{proof}
\end{lemma}
\begin{lemma} \label{lemI.5}
	Let $X,Y$ be $G$-sets. Then $$\Hom_G(X,Y)=\Maps(X,Y)^G.$$
	\begin{proof} We have
		\begin{align*}
			& f \in \Hom_G(X,Y) \\ \Leftrightarrow & \forall x \in X, g \in G: f(g.x)=g.f(x) \\ \Leftrightarrow & \forall g \in G, x \in X: g^{-1}f(g.x)=g^{-1}.(g.f(x))=f(x) \\
			\Leftrightarrow & \forall g \in G, x \in X: gf(g^{-1}x)=f(x) \\
			\Leftrightarrow & f \in Maps(X,Y)^G
		\end{align*}
	\end{proof}
\end{lemma}
\begin{definition}
	Let $X$ be a $G$-set and $k$ a field (or a commutative ring with $1$). A map $f: X \to k$ is $G$-invariant if $f(g)=f(g.x)$ for all $g \in G, x \in X$. 
\end{definition}
\begin{example}
	$G=\Z/2\Z=\{e,s\}$. Let $G$ act on $X=\R$ by $s.\lambda = -\lambda$ ($\lambda \in \R$). Let $k= \R$. Any polynomial $\sum_{i=0}^\infty a_it^i = p(t) \in \R[t]$ can be viewed as an element in $\Maps(\R,\R) = \Maps(X,R)$. 

	Then $p(t)$ is $G$-invariant $\Leftrightarrow p(t)$ is even (i.e. $a_i=0$ for all $i$ odd)
	
	\begin{proof}
		\begin{align*}
			& p(t) \text{ is } G-\text{inv.} \\ \Leftrightarrow & \forall \lambda \in \R: p(s.\lambda)=p(\lambda) \\
			\Leftrightarrow & \forall \lambda \in \R: p(-\lambda)=p(\lambda) \\
			\Leftrightarrow & \forall \lambda \in \R: \sum_{i \text{ odd}} (-1)^i a_i \lambda^i = \sum_{i \text{ odd}} a_i\lambda^i \\ 
			\Leftrightarrow & \forall \lambda \in \R: 2 \sum_{i \text{ odd}}  a_i\lambda^i = 0 \\
			\Leftrightarrow & \forall i \text{ odd:} a_i=0 \\
			\Leftrightarrow & p(t) \text{ is even.}
		\end{align*}
	\end{proof}
\end{example}
\begin{remark}
	$f$ is $G$-invariant $\Leftrightarrow f \in \Maps(X,k)^G$ where we have trivial $G$-action on $k$.
\end{remark}
\begin{lemma} \label{lemI.6} (Universal property of orbit sets)
	Let $X$ be a $G$-set, $k$ a field (or commutative ring with $1$). Then $f: X \to k$ is $G$-invariant $\Leftrightarrow f$ factors through $\can$ (i.e. $\exists ! \overline f: \orbit{G}{X} \to k$ such that $f = \overline f \circ \can$).
	
\begin{tikzcd}
& X \arrow{r}{f} \arrow{d}{\can} & k \\
& \orbit{G}{X}\arrow[swap]{ur}{\exists ! \overline f}
\end{tikzcd}
\begin{proof}
	\begin{align*}
		& f \text{ is } G-\text{invariant} \\
		\Leftrightarrow & \forall g \in G, x \in X: f(g.x)=f(x) \\
		\Leftrightarrow & f \text{ is constant on orbits} \\
		\Leftrightarrow & \overline f \text{ exists, namely } \overline f (\mathcal{O_x})=f(x)
	\end{align*}
\end{proof}
\end{lemma}
\begin{lemma} \label{lemI.7}
	Let $X$ be a finite $G$-set and $k$ a field (or commutative ring with $1$). Then
	\begin{enumerate}[label=(\roman*)]
		\item $\Maps(X,k)$ is a $k$-vsp (or $k$-module) with pointwise addition and scalar multiplication.
		\item A $k$-basis of $\Maps(X,k)^G$ is given by $\chi_y, y \in X$ where $$\chi_y=\begin{cases} 1, &\text{if } y= z \\ 0, &\text{otherwise} \end{cases}$$
		\item $\Maps(X,Y)^G$ form a subspace (or a submodule) with basis $\chi_\mathcal{O}, \mathcal{O} \in \orbit{G}{X}$ where $$\chi_\mathcal{O}(y)=\begin{cases}1, &\text{if } y \in \mathcal{O} \\ 0, & \text{otherwise}\end{cases}$$
	\end{enumerate} 
	\begin{proof}
		\begin{enumerate}[label=(\roman*)]
			\item clear
			\item Generating set: Let $f \in \Maps(X,k)$. Then $f= \sum_{y \in X} f(y) \chi_y$ because $\sum_{y \in X} f(y) \chi_y(x)=f(x)$ for alle $x \in X$. 
			
			Linear independence: $\sum_{y \in X} a_y \chi_y = 0$ gives $\sum a_y \chi_y(z) =0$ for all $z \in X$ and therefore $a_z=0$ for all $z \in X$.
			\item To show: $f_1, f_2, f \in \Maps(X,k)^G, \lambda \in \R:  f_1+f_2, \lambda f$ check it!
			
			Generating set: $f \in Maps(X,k)^G$ means that $f$ is constant on orbits and therefore $f = \sum a_\mathcal{O} \chi_\mathcal{O}$ with $a_\mathcal{O}=f(x)$ with $x \in \mathcal{O}$.
			
			Linear independence: See $(ii)$.
		\end{enumerate} 
	\end{proof}
\end{lemma}
If $X$ is an infinite set we replace $\Maps(X,k)$ by $$kX:=\{f: X \to k | \supp(f) \text{ is finite}\}$$ where $$\supp(f):=\{x \in X: f(x) \neq 0\}$$ is the support of $f$.
\begin{note}
	$\supp(f_1+f_2) \subset \supp(f_1) \cup \supp(f_2)$ 
	
	$\supp (\lambda f) \subset \supp (f)$
	
	for all $f_1,f_2,f \in \Maps(X,k)$
\end{note}
This gives that $kX \subset \Maps(X,k)$ is a vector subspace (usually just call is $kX$ as well). 

$kX$ is preserver under $G$-action. Let $f \in kX$ and $g \in G$. Then. $(g.f)(x) \neq 0 \Leftrightarrow f(g^{-1}(x)) \neq 0 \Leftrightarrow g^{-1}.x \in \supp(f)$ 

\Cref{lemI.7} generalizes to $kX$.
\begin{lemma} \label{lemI.8}
	Let $G$ be a group and $R$ a ring. Let $G$ act on $R$ by ringhomos (ie. if $\pi: G \times T \to R$ is the action, then $\pi_g: R \to R$ is a ringhomo for all $g \in G$), then $R^G$ is a subring of $R$.
	\begin{proof}
		Let $r_1,r_2 \in R^G$: To show: $r_1+r_2,r_1r_2 \in R^G$. For $g \in G$, we have $g.(r_1+r_2) = \pi_g(r_1+r_2)=\pi_g(r_1)+\pi_g(r_2)=gr_1+gr_2$. Similarly, $g.(r_1r_2)=r_1r_2$
	\end{proof}
\end{lemma}
\begin{example}
	Even polynomials form a subring of $\R[t]$.
\end{example}
\begin{definition}
	Let $G,H$ be groups and $X$ a $G$-set and an $H$-set. Then the two actions commute if $$g.(h.x)=h.(g.x)$$ for alle $g \in G, h \in H, x \in X$. In this cases $G \times H$ acts naturally via $(g,h).x=g.(h.x)$.
\end{definition}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
