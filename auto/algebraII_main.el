(TeX-add-style-hook
 "algebraII_main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("inputenc" "utf8") ("fontenc" "T1") ("setspace" "onehalfspacing") ("datetime2" "useregional" "showdow")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "algebraII_2018-10-08"
    "algebraII_2018-10-11"
    "algebraII_2018-10-15"
    "article"
    "art10"
    "babel"
    "inputenc"
    "fontenc"
    "lmodern"
    "amsmath"
    "amssymb"
    "amsthm"
    "fancyhdr"
    "enumitem"
    "geometry"
    "cleveref"
    "setspace"
    "pgf"
    "tikz"
    "tikz-cd"
    "mathrsfs"
    "stmaryrd"
    "titlesec"
    "datetime2")
   (TeX-add-symbols
    '("orbit" 2)
    "C"
    "R"
    "N"
    "Z"
    "Hom"
    "Maps"
    "id"
    "can"
    "supp"
    "GL"
    "End"
    "im"
    "chara"
    "F"
    "spann"
    "sectionbreak")
   (LaTeX-add-amsthm-newtheorems
    "lemma"
    "theorem"
    "corollary"
    "proposition"
    "definition"
    "example"
    "note"
    "remark"))
 :latex)

