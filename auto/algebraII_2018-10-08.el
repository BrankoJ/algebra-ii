(TeX-add-style-hook
 "algebraII_2018-10-08"
 (lambda ()
   (LaTeX-add-labels
    "lemI.1"
    "lemI.2"
    "lemI.3"
    "lemI.4"
    "lemI.5"
    "lemI.6"
    "lemI.7"
    "lemI.8"))
 :latex)

