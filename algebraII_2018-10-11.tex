\section{Representation of groups}

\begin{definition}
  Let $G$ be a group, $V$ a $k$-vectorspace, and let $G$ act on $V$.
  This action is called \textbf{linear} iff, $\pi_g \colon V \longrightarrow V$ is a linear map for all $g \in G$.
  Then $V$ is called a $G$-space, or a \textbf{representation} of $G$.
\end{definition}

\begin{example}
  If $V$ is a $k$-vectorspace, then $\GL (V)$ acts linearly on V by $g.v = g(v)$ for $g \in G$ and $v \in V$.
\end{example}

\underline{Standard representation}

\begin{remark}
  \begin{align*}
    \text{linear $G$-actions} \leftrightarrow & \text{group homos} \varphi \colon G \rightarrow \GL (V) \\
    \pi \mapsto & (g \mapsto \pi_g)
  \end{align*}
\end{remark}

\begin{example}
  \begin{enumerate}
  \item
    $X$ is a $G$-set.
    Then $kX$ is a representation of $G$ via $g.(\sum_{y\in C} a_y \chi_y )= \sum_{y \in X} a_y \chi_{g.y}$.
    (almost all $a_y = 0$)
    \underline{regular representation on $kX$}
  \item
    $V,W$ represetnations of $G$ over $k$, then the $G$-action on $\Maps (V,W)$ induces a $G$-action on $\Hom_k (V,W) := \{ f\colon V \rightarrow W \mid f k\text{-linear} \}$
  \item
    $V,W$ representations of $G$ over $k$.
    Then $V \oplus W$ and $V \otimes W$ are representations of $G$, called \textbf{direct sum} and \textbf{tensor product}, via $g.(v,w) = (g.v,g.w)$ and $g.(v \otimes w) = (g.v) \otimes (g.w)$ extended linearly for $g \in G$ and $v \in V$, $w \in W$.
  \end{enumerate}
\end{example}

\begin{definition}
  $V$ representation of $G$ over $k$.
  A \textbf{subrepresentation} of $V$ is a vector subspace $U$ of $V$ such that $g.u \in U$ for all $g \in G$ and $u \in U$.
  It is \textbf{proper} if $U \neq V$ and $U \neq 0$.
  It is \textbf{irreducible} if $V\neq 0$ and there exist no proper subrepresentations.
  It is \textbf{indecomposable}, if there is no decomposition $V = U_1 \oplus U_2$ where $U_1,U_2$ are proper subrepresentations.
  $V$ is \textbf{completely reducible} if $V = \bigoplus_{i\in I} V_i$, where $V_i$ are irreducible subrepresentations.
\end{definition}

\begin{remark}
  \underline{Clear:} irreducible $\implies$ indecomposable.
  The converse is not true in general.

  \underline{Example:}
  $G = \{
  \begin{pmatrix}
    a & b \\
    c & 0
  \end{pmatrix}
  \mid a,b,c \in \C, a,c \neq 0\}$
  acts on $V = \C^2$ by standard action.
  Then $U := < \binom{1}{0}>$ is a \textbf{proper subrepresentation} of $V$.
  It follows that $V$ is not irreducible.
  $V$ is indecomposable, since $U$ is the unique proper subrepresentation.
  \textit{Explanation: Assume $U^\prime = <\binom{x}{-y}>$ proper subrepresentation.
    Then $\begin{pmatrix} 1 & 1 \\ 0 & 1\end{pmatrix} \binom{x}{y} = \binom{x+y}{y} \in U^\prime$.  Then $\binom{y}{0} \in U^\prime$, which imples $U^\prime = Y$.
    V is also not completely reducible.}
\end{remark}

\begin{definition}
  Let $G$ be a group and $k$ a field.
  The \textbf{group algebra} of $G$ over $k$ is the $k$-algebra given by the $k$-vectorspace
  \begin{equation*}
    kG = \{\, f \colon G \rightarrow k \mid \supp (f) \text{ is finite} \, \}
  \end{equation*}
  with the multiplication of functions
  \begin{equation*}
    (f_1 \cdot f_2)(x) = \sum_{y \in G} f_1 (y) f_2 (y^{-1}x)
  \end{equation*}
  with unit $1 = \chi_e$.
  \textit{(Indeed: $(f \cdot \chi_e)(x) = \sum_y f(y)\chi_e (y^{-1}x) = f(x)$ and $\chi_e(y)f(y^{-1}x) = f(x)$ for all $f \in kG$.)}
  To check: associativity and distributivity.
  \begin{remark}
    The group algebra can be defined in the same way over any commutative ring with one.
    We write
    \begin{equation*}
      \sum_{g \in G} a_g g := \sum_{g \in G} a_g \chi_g
    \end{equation*}
    where $a_g \in k$ and almost all $a_g = 0$.
  \end{remark}
\end{definition}

\begin{lemma} \label{lemii1}
  The algebra structure on $kG$ is given by extending bilinearly te multiplication on $G$.
\end{lemma}

\begin{proof}
  \begin{align*}
    \chi_g \cdot \chi_h (x) = \sum_{y \in G} \chi_g(y)\chi_h(y^{-1}x) = \begin{cases}1 & \text{if $h = g^{-1}x = chi_{gh} (x)$} \\ 0 & \text{otherwise} \end{cases}
  \end{align*}
  By definition the convolution product extends this bilinearly.
\end{proof}

\begin{note}
  $kG$ is commutative if and only of $G$ is commutative/abelian.
\end{note}

\begin{lemma} \label{lemii2}
  Let $G$ be a group , $V$ a vectorspace.
  Then
  \begin{align*}
    \{ \text{linear $G$-actions on $V$} \} & \leftrightarrow \{ \text{$kG$-module structures on $V$}\} \\
    (\pi\colon G\times V \rightarrow V) & \mapsto \left( (\sum_{g \in G} a_g g ).v := \sum_{g \in G} a_g (g.v) \right)
  \end{align*}
\end{lemma}

\begin{proof}
  Check yourself.
\end{proof}

\begin{definition}
  Let $V,W$ representations of $G$ over $k$.
  A \textbf{morphism} of representations from $V$ to $W$ is a linear, $G$-equivariant map $f \colon V \rightarrow W$.
  Denote $\Hom_G(V,W) := \{\, f \colon V \rightarrow W \text{ morphism of representations}\,\}$, $\End_G(V) := \Hom_G(V,V)$.
\end{definition}

\begin{note}
  $\Hom_G(V,W)$ is a vector space.
  Write $V \cong W$ if there exists an isomorphism $f \colon V \rightarrow W$.
\end{note}

\begin{lemma} \label{lemii3}
  Let $G$ be a group and $k$ a field.
  The representations of $G$ over $k$ together with morphisms of representations form a category, which we call $\operatorname{Rep_k} (G)$, that is $\forall V \text{ representation of } G : \id_V \in \End_G(V) \, , \; \forall V,W,Z \text{ representations of } G \text{ and } f \in \Hom_G(V,W), \, f_2 \in \Hom_G (W,Z) : f_2 \circ f_1 \in \Hom_G(V,Z)$.  
\end{lemma}
\begin{proof}
  See \Cref{lemI.2}.
\end{proof}

\begin{example}
  For a field $k$, $k$-vectorspaces together with $k$-linear maps form a category $\operatorname{Vect}_k$.
\end{example}

\begin{corollary}
  Let $k$ be a field.
  The assignments
  \begin{align*}
    F\colon V & \longmapsto V^G \\
    f & \longmapsto f^G \colon V^G \rightarrow W^G \\
    \text{(representation $G$ over $k$)} &\phantom{\longrightarrow} \text{(linear map)}
  \end{align*}
  Define a functor from $\operatorname{Rep}_k$ to $\operatorname{Vect}_k$, that is $F(f_1 \circ f_2) = F(f_1) \circ F(f_2)$, $F(\id_{F(V)}$ for all representations $V$ of $G$ and $f_1,f_2$ morphisms of representations whenever $f_1 \circ f_2$ makes sense.
\end{corollary}
\begin{proof}
  See last lecture and ``linear''.
\end{proof}

$F$ is the functor of $G$-invariants.

\begin{lemma} \label{lemii5}
  If $f \colon V \rightarrow W$ is a morphism of representations of $G$, then $\ker f$ and $\im f$ are subrepresentations of $V$ respectively of $W$.
\end{lemma}

\begin{proof}
  $\ker f$, $\im f$ are subspaces, because $f$ is linear.
  Let $g \in G$ and $x \in \ker f$.
  Then
  \begin{align*}
    f(g.x) = g.(f(x)) = g.0 = 0 \\
    \implies g.x \in \ker f \implies \ker f \text{subrepresentation}
  \end{align*}
  Let $ y \in \im f$, $ y = f(x)$ for some $x \in V$.
  Then $g.y = g.(f(x)) = f(g.x) \in \im f$.
\end{proof}

\begin{remark}
  It can be shown that $\operatorname{Rep}_k(G)$ is an abelian category.
\end{remark}

\begin{lemma}[Schur's Lemma] \label{schur}
  let $G$ be a group and $V,W$ irreducible representations of $G$ over $k$.
  \begin{enumerate}
  \item
    \begin{align*}
      \Hom_G(V,W) = 0 & \text{if } V \not\cong W  \\  
      \Hom_G(V,W) \neq 0 & \text{if } V \cong W  
    \end{align*}
    and then every morphism is an isomorphism
  \item
    If $k = \bar{k}$ and $V,W$ are finite dimensional, then
    \begin{equation*}
      Hom_G(V,W) \cong \begin{cases} k & \text{if } V \cong W  \\  0 & \text{if } V \not\cong W\end{cases}
    \end{equation*}
  \end{enumerate}
\end{lemma}

\begin{proof}
  \begin{enumerate}
  \item
    Assume $V \not\cong W$ and $ 0 \neq f \in Hom_G(V,W)$.
    Then $\ker f \neq V$ and $\im f \neq 0$.
    Then \cref{lemii5} implies $ker f = 0$ since $V$ is irreducible and $\im f = W$ since $W$ is irreducible.
    Then $f$ is an isomorphism, which is a contradiction to $V \not\cong W$.

    If $V \cong W$ then $\Hom_G (V,W) \neq 0$ by definition.
    Then automatically every non-zero morphism is an isomorphism by the previous paragraph.
  \item
    Assume $V \cong W$ and $\alpha, \beta \in \Hom_G(V,W)$.
    Enough to show $\beta = \lambda \alpha$ for some $\lambda \in k$.
    By 1), $\alpha$ has an inverse $\alpha^{-1}$, which is again a morphism.
    Then $\alpha^{-1} \circ \beta \in \End_G(V)$.
    If $k = \bar{k}$, and $V$ is finite dimensional, then $\alpha^{-1} \circ \beta$ has an eigenvalue.
    This implies $K := \ker (\alpha^{-1} \circ \beta - \lambda \id_V) \neq 0$ for some $\lambda \in k$.
    Now $\alpha \circ \beta - \lambda \id_V \in \End_(V)$ (check!!), thus $K$ is a subrepresentation of $V$, hence $V=K$ since $V$ is irreducible and $K \neq 0$.
    It follows that $\alpha^{-1} \circ \beta = \lambda \id_V$ and therefore $\beta = \lambda \alpha$.
  \end{enumerate}  
\end{proof}

\begin{corollary}
  If $k = \bar{k}$ and $V_i$, $1\leq i \leq r$ finite-dimensional irreducible pairwise non-isomorphic representations of $G$ over $k$.
  Let $W_i := V_i^{\oplus n_i} := V_i \oplus \ldots \oplus V_i$ ($n_i$ copies) for some $n_i \in \Z_{\>0}$ (a representation of $G$).
  Then $\End_G(W_1 \oplus \ldots \oplus W_r) \cong M_{n_1 \times n_1}(k) \oplus \ldots \oplus M_{n_r \times n_r}(k)$ as algebras/rings.
\end{corollary}
\begin{proof}
  \begin{align*}
    &End_G (W_1 \oplus \ldots \oplus W_r)  \\
    \cong& \Hom_G(V_1^{\oplus n_1} \oplus \ldots \oplus V_r^{\oplus n_r})  \\
    \cong& \End_G(V_1^{n_1}) \oplus \ldots \oplus \End_G(V_r^{\oplus n_r})  \\
    \cong& M_{n_1 \times n_1}(k) \otimes \ldots \otimes M_{n_r \times n_r}(k)
  \end{align*}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:
